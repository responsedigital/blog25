class Blog25 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse(this.querySelector('script[type="application/json"]').text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initBlog25()
    }

    initBlog25 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: blog25")
    }

}

window.customElements.define('fir-blog-25', Blog25, { extends: 'section' })
