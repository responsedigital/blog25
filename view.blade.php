<!-- Start blog25 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A pagination bar for posts - round square boxes. -->
@endif
<section class="{{ $block->classes }}" is="fir-blog-25" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}" data-observe-resizes>
  <div class="blog-25 {{ $theme }} {{ $text_align }} {{ $flip }}">
    @php
      echo paginate_links(array(
        'base' => preg_replace('/\?.*/', '', get_pagenum_link()) . '%_%',
        'format' => '?news-page=%#%',
        'current' => $paged,
        'prev_next' => false,
        'total' => $total
      ));
    @endphp
  </div>
</section>
<!-- End blog25 -->
