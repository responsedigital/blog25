module.exports = {
    'name'  : 'blog25',
    'camel' : 'Blog25',
    'slug'  : 'blog-25',
    'dob'   : 'Blog_25_1440',
    'desc'  : 'A pagination bar for posts - round square boxes.',
}