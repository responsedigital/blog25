<?php

namespace Fir\Pinecones\Blog25\Blocks;

use Log1x\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Fir\Utils\GlobalFields as GlobalFields;
use function Roots\asset;

class Blog25 extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Blog25';

    /**
     * The block view.
     *
     * @var string
     */
    public $view = 'Blog25.view';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = '';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'fir';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'editor-ul';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = ['blog25'];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => array('wide', 'full', 'other'),
        'align_text' => false,
        'align_content' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $defaults = [
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        $data = $this->parse(get_fields());
        $newData = [
        ];

        return array_merge($data, $newData);
    }

    private function parse($data)
    {
        $data['flip'] = $data['options']['flip_horizontal'] ? 'blog-25--flip' : '';
        $data['text_align'] = 'blog-25--' . $data['options']['text_align'];
        $data['theme'] = 'fir--' . $data['options']['theme'];
        return $data;
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {

        $Blog25 = new FieldsBuilder('blog25');

        $Blog25
            ->addGroup('content', [
                'label' => 'blog25',
                'layout' => 'block'
            ])
            ->endGroup()
            ->addGroup('options', [
                'label' => 'Options',
                'layout' => 'block'
            ])
            ->addFields(GlobalFields::getFields('flipHorizontal'))
            ->addFields(GlobalFields::getFields('textAlign'))
            ->addFields(GlobalFields::getFields('theme'))
            ->addFields(GlobalFields::getFields('hideComponent'))
            ->endGroup();

        return $Blog25->build();
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        wp_enqueue_style('sage/app.css', asset('styles/fir/Pinecones/Blog25/style.css')->uri(), false, null);
    }
}
